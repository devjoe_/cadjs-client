import * as ACT from './reducers/actions';

var sec = "wss:";
if(window.location.protocol === "http:") sec = "ws:";
let connection;
let tries = 1;
let still_connecting = true;

function connect(dispatch) {
    //connection = new WebSocket(sec+"//"+window.location.host+"/ws");
    connection = new WebSocket(sec+"//"+window.location.hostname+":8080/ws"); // For testing. React proxying is not working properly.

    connection.onopen = function() {
        console.log('[WEBSOCKET] Connected.');
    }

    connection.onmessage = function(e) {
        console.log('[WEBSOCKET] '+e.data);
        let json = JSON.parse(e.data);
        if(still_connecting) {
            tries = 1;
            dispatch(ACT.doneConnecting(json['_connectionID'], json.data.restricted));
            if(json.data.setupMode) {
                dispatch(ACT.enterSetupMode());
            }
        }
    }

    connection.onerror = function(e) {
        console.error('[WEBSOCKET] ERROR!');
        console.error(e);
    }

    connection.onclose = function() {
        console.log('[WEBSOCKET] Disconnected.');
        tries++;
        if(tries < 6) {
            console.log('[WEBSOCKET] Attempting to reconnect. Attempt '+tries+' / 5.');
            dispatch(ACT.setConnectionAttempt(tries, 5))
            connect(dispatch);
            return;
        }
        console.error('[WEBSOCKET] Complete connection failure.');
        dispatch(ACT.cannotConnect());
    }
}
function sendWSMessage(payload, data) {
    connection.send(JSON.stringify({payload: payload, data: data}));
}

export default {
    connect,
    websocket: () => connection,
    sendWSMessage
}
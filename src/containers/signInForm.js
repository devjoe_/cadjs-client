import React, { Component } from 'react';

class signInForm extends Component {
    render() {
        return <div className="container">
            <div className="row mt-5">
                <div className="col-md-4 center">
                    <h1>Sign in</h1>
                    <form>
                        <div className="form-group">
                            <label htmlFor="email">Email Address</label>
                            <input type="email" id="email" className="form-control" placeholder="john.doe@example.com" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" className="form-control" placeholder="s3cret" />
                        </div>
                        <button className="btn btn-primary">Sign in</button>
                        <button className="btn btn-outline-secondary ml-1">Request Access</button>
                    </form>
                    <small>CID: { this.props.connID }</small>
                </div>
            </div>
        </div>;
    }
}

export default signInForm;
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import cadjs_client from './reducers';
import {
    BrowserRouter as Router,
    Route
  } from 'react-router-dom'

const store = createStore(cadjs_client);

ReactDOM.render(<Provider store={store}>
<Router>
    <App />
</Router>
</Provider>, document.getElementById('root'));
registerServiceWorker();

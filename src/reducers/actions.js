export const STORE_USER = 'STORE_USER';
export const DELETE_USER = 'DELETE_USER';

export function storeUser(user) {
    return {type: STORE_USER, user};
}
export function deleteUser() {
    return {type: DELETE_USER}
}

// ---

export const DONE_CONNECTING = 'DONE_CONNECTING';
export const CANNOT_CONNECT = 'CANNOT_CONNECT';
export const SET_CONNECTION_ATTEMPT = 'SET_CONNECTION_ATTEMPT'

export function doneConnecting(connID, restricted) {
    return {type: DONE_CONNECTING, connID: connID, restricted: restricted};
}

export function cannotConnect() {
    return {type: CANNOT_CONNECT};
}

export function setConnectionAttempt(attemptNo, of) {
    return {type: SET_CONNECTION_ATTEMPT, attemptNo: attemptNo+'/'+of}
}

// ---

export const ENTER_SETUP_MODE = 'ENTER_SETUP_MODE';
export const EXIT_SETUP_MODE = 'EXIT_SETUP_MODE';

export function enterSetupMode() {
    return {type: ENTER_SETUP_MODE};
}

export function exitSetupMode() {
    return {type: EXIT_SETUP_MODE};
}
import {combineReducers} from 'redux';
import {STORE_USER, DELETE_USER, DONE_CONNECTING, CANNOT_CONNECT, SET_CONNECTION_ATTEMPT
, ENTER_SETUP_MODE, EXIT_SETUP_MODE} from './actions';

function user(state = null, action) {
    switch(action.type) {
        case STORE_USER:
            return action.user;
        case DELETE_USER:
            return null;
        default:
            return state;
    } 
}

function connected(state = {connected: false, failed: false, attempt: '1/5', restricted: false, connID: -1}, action) {
    let newstate = Object.assign({}, state);
    switch(action.type) {
        case DONE_CONNECTING:
            newstate.connected = true;
            newstate.restricted = action.restricted;
            newstate.connID = action.connID;
            return newstate;
        case CANNOT_CONNECT:
            newstate.connected = false;
            newstate.failed = true;
            return newstate;
        case SET_CONNECTION_ATTEMPT:
            newstate.attempt = action.attemptNo;
            return newstate;
        default:
            return state;
    }
}

function setupMode(state = false, action) {
    switch(action.type) {
        case ENTER_SETUP_MODE:
            return true;
        case EXIT_SETUP_MODE:
            return false;
        default:
            return state;
    }
}

const cadjs_client = combineReducers({
    user,
    connected,
    setupMode
});

export default cadjs_client;
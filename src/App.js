import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {doneConnecting, cannotConnect, setConnectionAttempt} from './reducers/actions';
import { connect } from 'react-redux';
import SignInForm from './containers/signInForm';
import SH from './SocketHandler';
import spinner from './spinner.gif';
import {Link, withRouter, Route} from 'react-router-dom';
import setupForm from './containers/setupForm';

class App extends Component {
  constructor() {
    super();
    this.state = {bypassSetupModal: false}
    this.BP = this.BP.bind(this);
  }
  componentDidMount() {
    SH.connect(this.props.dispatch); 
  }
  BP() {
    this.setState({bypassSetupModal: true});
    this.props.history.push('/setup');
  }
  render() {
    if(this.props.connDets.connected === false && this.props.connDets.failed === false) {
      return <div className="container">
        <div className="row mt-5">
          <div className="col-md-9 center">
            <div className="alert alert-info" role="alert">
              <div className="media">
                <img className="mr-3" src={spinner} height="50" />
                <div className="media-body">
                  <h5 className="mt-0">Securely connecting you...</h5>
                  Please wait while we connect you to the CADJS servers.
                  <br />
                  <small>Attempt { this.props.connDets.attempt }</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    } else if(this.props.connDets.failed === true) {
      return <div className="container">
        <div className="row mt-5">
          <div className="col-md-9 center">
            <div className="alert alert-danger" role="alert">
              <h5>Connection failed.</h5>
              <hr />
              The CADJS servers appear to be temporarily unavailable.
            </div>
          </div>
        </div>
      </div>
    } else if(this.props.connDets.restricted) {
      return <div className="container">
        <div className="row mt-5">
          <div className="col-md-9 center">
            <div className="alert alert-danger" role="alert">
              <h5>Restricted</h5>
              <hr />
              The CADJS instance is locked for setup. If you own this instance and are not setting it up, it is recommended that you shut the instance down and remove it from public view.
            </div>
          </div>
        </div>
      </div>
    } else if(this.props.setupMode && !this.state.bypassSetupModal) {
      return <div className="container">
        <div className="row mt-5">
          <div className="col-md-9 center">
            <div className="alert alert-info" role="alert">
              <h5>Setup Mode</h5>
              <hr />
              <p>The CADJS instance is recognising this connection as the master for setup. Click the button below to proceed into setup mode.</p>
              <button onClick={this.BP} className="btn btn-primary">Enter setup mode</button>
            </div>
          </div>
        </div>
      </div>
    }
    if(this.props.user == null && !this.props.setupMode) {
      return <SignInForm connID={this.props.connDets.connID} />
    }

    // Router
    return (<div>
      <Route component={setupForm} path={"/setup"} exact />
    </div>)
  }
}

let mstp = (state) => ({
  user: state.user,
  connDets: state.connected,
  setupMode: state.setupMode
});

export default connect(mstp)(withRouter(App));
